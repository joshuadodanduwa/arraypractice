let language = ["Kia ora", "Kon'nichiwa",
               "Namaste", "Bonjour", "Talofa"]

let input = 0


input = Number(prompt("I can translate 'hello' into 5 different languages:\n" +
                "1. Maori\n" +
                "2. Japanese\n" +
                "3. Hindi\n" +
                "4. French\n" +
                "5. Samoan\n" +
                "\n" +
                "Which language would you like me to translate 'hello' into (1 to 5)?"))

confirm(`Your selected translation : ${input}\n`+
      `Hello is translated as : ${language[input-1]}`)
